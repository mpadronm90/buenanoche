﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuenaNocheGenNHibernate.EN.Default_;
using BuenaNocheGenNHibernate.CEN.Default_;

namespace BuenaNocheCP.CPs
{
    public class ClienteCP : BasicCP
    {
        private ClienteCEN clienteCEN;

        public ClienteEN dameClientePorDNI(String dni)
        {
            try
            {
                SessionInitializeTransaction();
                clienteCEN = new ClienteCEN();
                return clienteCEN.ReadOID(dni);
            }
            catch
            {
                SessionRollBack();
                return null;
            }
        }
        public List<ClienteEN> dameClienteDNIParcial(String dniParcial)
        {
            List<ClienteEN> l = null;
            try
            {
                SessionInitializeTransaction();
                l = (List<ClienteEN>)clienteCEN.ReadAll(0, 0);

                foreach (ClienteEN cli in l)
                {
                    if (!cli.Nif.Contains(dniParcial))
                        l.Remove(cli);
                }
                SessionCommit();
            }
            catch
            {
                SessionRollBack();
            }

            return l;
        }

        public List<ClienteEN> dameClientePorNombre(String nombre)
        {
            List<ClienteEN> l = new List<ClienteEN>();
            if (nombre.Length == 0)
                return l;
            try
            {
                SessionInitializeTransaction();
                l = (List<ClienteEN>)clienteCEN.ReadAll(0, 0);

                foreach (ClienteEN cli in l)
                {
                    if (!cli.Nombre.Contains(nombre))
                        l.Remove(cli);
                }
                SessionCommit();
            }
            catch
            {
                SessionRollBack();
            }
            return l;
        }

        public Boolean guardaNuevoCliente(string nif, string nombre, string telefono, string localidad, string provincia, string pais) {
            try
            {
                SessionInitializeTransaction();
                clienteCEN = new ClienteCEN();
                clienteCEN.New_(nif, nombre, telefono, localidad, provincia, pais);
                SessionCommit();
            }
            catch (Exception ex)
            {
                SessionRollBack();
                return false;
            }
            return true;
        }

        public Boolean guardaCliente(string nif, string nombre, string telefono, string localidad, string provincia, string pais) {
            try
            {
                SessionInitializeTransaction();
                clienteCEN.Modify(nif, nombre, telefono, localidad, provincia, pais);
                SessionCommit();
            }
            catch (Exception ex)
            {
                SessionRollBack();
                return false;
            }
            return true;
        }

        public Boolean modificaNifCliente(String nifAntiguo, String nifNuevo)
        {
            try
            {
            SessionInitializeTransaction();
            ClienteEN clienteEN = (ClienteEN)session.Load(typeof(ClienteEN), nifAntiguo);

            clienteEN.Nif = nifNuevo;

            session.Update(clienteEN);
            SessionCommit();
            }

            catch (Exception ex) {
                    SessionRollBack ();
                    return false;
            }
            return true;
        }
    }
}
