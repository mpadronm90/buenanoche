﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuenaNocheGenNHibernate.EN.Default_;
using BuenaNocheGenNHibernate.CEN.Default_;

namespace BuenaNocheCP.CPs
{
    public class ReservaCP : BasicCP
    {

        /**
        * Busca las reservas que haya realizado un cliente.
        * Del cliente se sabe un String, este puede ser nombre o dni.
        * @param nombre_dni : String
        * @OrderBy fecha : descendente, de la más reciente a la más antigua.
        * @return Lista de Reservas hechas por el cliente.
        */
        //michel
        public List<ReservaEN> dameReservasPorCliente(String nombre_dni)
        {
            List<ReservaEN> reservas = null;
            ReservaCEN rsCEN = null;

            try
            {
                SessionInitializeTransaction();
                rsCEN = new ReservaCEN();
                reservas = (List<ReservaEN>)rsCEN.ReadForClient(nombre_dni);
                SessionCommit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                SessionRollBack();
            }

            return reservas;
        }

        /**
        * Busca una reserva concreta por clave
        * @param num : int
        * @return ReservaCEN
        */
        //michel
        public ReservaEN dameReservaPorOID(int num)
        {
            ReservaEN reserva = null;
            ReservaCEN rsCEN = null;

            try
            {
                SessionInitializeTransaction();
                rsCEN = new ReservaCEN();
                reserva = rsCEN.ReadOID(num);
                SessionCommit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                SessionRollBack();
            }

            return reserva;

        }

        public List<ReservaEN> dameReservasPorFecha(DateTime fecha)
        {
            List<ReservaEN> reservas = null;
            ReservaCEN rsCEN = null;

            try
            {
                SessionInitializeTransaction();
                rsCEN = new ReservaCEN();
                reservas = (List<ReservaEN>)rsCEN.ReadForDate(fecha);
                SessionCommit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                SessionRollBack();
            }

            return reservas;
        }

        public Boolean guardaReserva(ClienteEN cliente, DateTime fechaEntrada, DateTime fechaSalida, HabitacionEN habitacion)
        {
            ReservaCEN rsCEN = null;
            ReservaEN rsEN = null;
            LineaReservaCEN lrsCEN = null;
            LineaReservaEN lrsEN = null;
            DiaEN diaEN = null;
            DiaCEN diaCEN = null;
            try
            {
                SessionInitializeTransaction();
                rsCEN = new ReservaCEN();
                rsEN = new ReservaEN();
                lrsCEN = new LineaReservaCEN();
                lrsEN = new LineaReservaEN();
                diaCEN = new DiaCEN();
                diaEN = new DiaEN();
                int cod = rsCEN.New_();
                int codLinea = lrsCEN.New_();
                rsEN = (ReservaEN)session.Get(typeof(ReservaEN), cod);
                lrsEN = (LineaReservaEN)session.Get(typeof(LineaReservaEN), codLinea);
                rsEN.Cliente = cliente;
                lrsEN.Reserva = rsEN;
                lrsEN.Habitacion = habitacion;
                diaCEN.New_(fechaSalida);
                diaEN = (DiaEN)session.Get(typeof(DiaEN), fechaSalida);
                lrsEN.Dia = diaEN;
                rsEN.LineasReservaReservas.Add(lrsEN);
                SessionCommit();
            }
            catch
            {
                SessionRollBack();
                return false;
            }
            return true;
        }
    }
}
