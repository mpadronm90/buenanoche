﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuenaNocheGenNHibernate.EN.Default_;
using BuenaNocheGenNHibernate.CEN.Default_;

namespace BuenaNocheCP.CPs
{
    public class HabitacionCP : BasicCP
    {

        /** 
         * Lista las habitaciones.         
         */
        // Michel
        public List<HabitacionEN> listadoHabitaciones() {

            List<HabitacionEN> listadoHabitaciones = null;
            HabitacionCEN hbCEN = null;
            try
            {
                SessionInitializeTransaction();
                listadoHabitaciones = new List<HabitacionEN>();
                hbCEN = new HabitacionCEN();
                listadoHabitaciones = (List<HabitacionEN>)hbCEN.Listar(0, 0);
                SessionCommit();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                SessionRollBack();
            }
            return listadoHabitaciones;

        }

        /** 
      * Lista las habitaciones por categoria. Parametro 1: Nombre de categoria        
      */
        // Michel
        public List<HabitacionEN> listadoHabitacionesPorCategoria(String Categoria)
        {
            List<HabitacionEN> listadoHabitaciones = null;
                   
                listadoHabitaciones = this.listadoHabitaciones();
               
                foreach (HabitacionEN hab in listadoHabitaciones)
                {
                    if (!hab.Categoria.Nombre.Equals(Categoria))
                        listadoHabitaciones.Remove(hab);
                }

            return listadoHabitaciones;
        }

        /**
         * Devuelve una habitación por su número.
         * @param num : int
         * @return HabitacionCEN
         */
        //michel
        public HabitacionEN dameHabitacionPorNum(int num)
        {
            HabitacionEN Habitacion = null;
            HabitacionCEN hbCEN = null;
            try
            {
                SessionInitializeTransaction();                
                hbCEN = new HabitacionCEN();
                Habitacion = hbCEN.Filtrar_por_id(num);
                SessionCommit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                SessionRollBack();
            }
            return Habitacion;
        }

        /**
         * Cambia la categoria de una habitacion.
         * @param num : int
         * @param categoria : CategoriaCEN
         * @return booleano : si se ha cambiado o si no
         */
        //michel
     /*   public Boolean cambiaCategoriaHabitacion(int num, CategoriaEN categoria)
        {
            Boolean cambio = false;
            HabitacionCEN hbCEN = null;
            try
            {
                SessionInitializeTransaction();
                hbCEN = new HabitacionCEN();
                cambio = hbCEN.get_IHabitacionCAD();
                //SessionCommit();
            }
            catch (Exception e)
            {
                SessionRollBack();
            }
            return cambio;
        }
        */
    

    }
}
