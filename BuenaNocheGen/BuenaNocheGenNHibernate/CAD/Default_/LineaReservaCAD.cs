
using System;
using System.Text;
using BuenaNocheGenNHibernate.CEN.Default_;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using BuenaNocheGenNHibernate.EN.Default_;
using BuenaNocheGenNHibernate.Exceptions;

namespace BuenaNocheGenNHibernate.CAD.Default_
{
public partial class LineaReservaCAD : BasicCAD, ILineaReservaCAD
{
public LineaReservaCAD() : base ()
{
}

public LineaReservaCAD(ISession sessionAux) : base (sessionAux)
{
}



public LineaReservaEN ReadOIDDefault (int id)
{
        LineaReservaEN lineaReservaEN = null;

        try
        {
                SessionInitializeTransaction ();
                lineaReservaEN = (LineaReservaEN)session.Get (typeof(LineaReservaEN), id);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is BuenaNocheGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new BuenaNocheGenNHibernate.Exceptions.DataLayerException ("Error in LineaReservaCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return lineaReservaEN;
}


public int New_ (LineaReservaEN lineaReserva)
{
        try
        {
                SessionInitializeTransaction ();

                session.Save (lineaReserva);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is BuenaNocheGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new BuenaNocheGenNHibernate.Exceptions.DataLayerException ("Error in LineaReservaCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return lineaReserva.Id;
}

public void Destroy (int id)
{
        try
        {
                SessionInitializeTransaction ();
                LineaReservaEN lineaReservaEN = (LineaReservaEN)session.Load (typeof(LineaReservaEN), id);
                session.Delete (lineaReservaEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is BuenaNocheGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new BuenaNocheGenNHibernate.Exceptions.DataLayerException ("Error in LineaReservaCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}

public void Modify (LineaReservaEN lineaReserva)
{
        try
        {
                SessionInitializeTransaction ();
                LineaReservaEN lineaReservaEN = (LineaReservaEN)session.Load (typeof(LineaReservaEN), lineaReserva.Id);
                session.Update (lineaReservaEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is BuenaNocheGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new BuenaNocheGenNHibernate.Exceptions.DataLayerException ("Error in LineaReservaCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}
}
}
