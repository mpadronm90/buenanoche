
using System;
using System.Text;
using BuenaNocheGenNHibernate.CEN.Default_;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using BuenaNocheGenNHibernate.EN.Default_;
using BuenaNocheGenNHibernate.Exceptions;

namespace BuenaNocheGenNHibernate.CAD.Default_
{
public partial class DiaCAD : BasicCAD, IDiaCAD
{
public DiaCAD() : base ()
{
}

public DiaCAD(ISession sessionAux) : base (sessionAux)
{
}



public DiaEN ReadOIDDefault (Nullable<DateTime> fecha)
{
        DiaEN diaEN = null;

        try
        {
                SessionInitializeTransaction ();
                diaEN = (DiaEN)session.Get (typeof(DiaEN), fecha);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is BuenaNocheGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new BuenaNocheGenNHibernate.Exceptions.DataLayerException ("Error in DiaCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return diaEN;
}


public Nullable<DateTime> New_ (DiaEN dia)
{
        try
        {
                SessionInitializeTransaction ();

                session.Save (dia);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is BuenaNocheGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new BuenaNocheGenNHibernate.Exceptions.DataLayerException ("Error in DiaCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return dia.Fecha;
}

public void Destroy (Nullable<DateTime> fecha)
{
        try
        {
                SessionInitializeTransaction ();
                DiaEN diaEN = (DiaEN)session.Load (typeof(DiaEN), fecha);
                session.Delete (diaEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is BuenaNocheGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new BuenaNocheGenNHibernate.Exceptions.DataLayerException ("Error in DiaCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}
}
}
