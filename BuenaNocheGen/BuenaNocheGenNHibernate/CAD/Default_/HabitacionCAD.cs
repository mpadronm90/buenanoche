
using System;
using System.Text;
using BuenaNocheGenNHibernate.CEN.Default_;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using BuenaNocheGenNHibernate.EN.Default_;
using BuenaNocheGenNHibernate.Exceptions;

namespace BuenaNocheGenNHibernate.CAD.Default_
{
public partial class HabitacionCAD : BasicCAD, IHabitacionCAD
{
public HabitacionCAD() : base ()
{
}

public HabitacionCAD(ISession sessionAux) : base (sessionAux)
{
}



public HabitacionEN ReadOIDDefault (int numero)
{
        HabitacionEN habitacionEN = null;

        try
        {
                SessionInitializeTransaction ();
                habitacionEN = (HabitacionEN)session.Get (typeof(HabitacionEN), numero);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is BuenaNocheGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new BuenaNocheGenNHibernate.Exceptions.DataLayerException ("Error in HabitacionCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return habitacionEN;
}


public int Nueva (HabitacionEN habitacion)
{
        try
        {
                SessionInitializeTransaction ();

                session.Save (habitacion);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is BuenaNocheGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new BuenaNocheGenNHibernate.Exceptions.DataLayerException ("Error in HabitacionCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return habitacion.Numero;
}

public void Eliminar (int numero)
{
        try
        {
                SessionInitializeTransaction ();
                HabitacionEN habitacionEN = (HabitacionEN)session.Load (typeof(HabitacionEN), numero);
                session.Delete (habitacionEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is BuenaNocheGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new BuenaNocheGenNHibernate.Exceptions.DataLayerException ("Error in HabitacionCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}

public void Modify (HabitacionEN habitacion)
{
        try
        {
                SessionInitializeTransaction ();
                HabitacionEN habitacionEN = (HabitacionEN)session.Load (typeof(HabitacionEN), habitacion.Numero);

                habitacionEN.Superficie = habitacion.Superficie;


                habitacionEN.Piso = habitacion.Piso;

                session.Update (habitacionEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is BuenaNocheGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new BuenaNocheGenNHibernate.Exceptions.DataLayerException ("Error in HabitacionCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}
public System.Collections.Generic.IList<HabitacionEN> Listar (int first, int size)
{
        System.Collections.Generic.IList<HabitacionEN> result = null;
        try
        {
                SessionInitializeTransaction ();
                if (size > 0)
                        result = session.CreateCriteria (typeof(HabitacionEN)).
                                 SetFirstResult (first).SetMaxResults (size).List<HabitacionEN>();
                else
                        result = session.CreateCriteria (typeof(HabitacionEN)).List<HabitacionEN>();
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is BuenaNocheGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new BuenaNocheGenNHibernate.Exceptions.DataLayerException ("Error in HabitacionCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return result;
}

public HabitacionEN Filtrar_por_id (int numero)
{
        HabitacionEN habitacionEN = null;

        try
        {
                SessionInitializeTransaction ();
                habitacionEN = (HabitacionEN)session.Get (typeof(HabitacionEN), numero);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is BuenaNocheGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new BuenaNocheGenNHibernate.Exceptions.DataLayerException ("Error in HabitacionCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return habitacionEN;
}
}
}
