
using System;
using BuenaNocheGenNHibernate.EN.Default_;

namespace BuenaNocheGenNHibernate.CAD.Default_
{
public partial interface IHabitacionCAD
{
HabitacionEN ReadOIDDefault (int numero);

int Nueva (HabitacionEN habitacion);

void Eliminar (int numero);


void Modify (HabitacionEN habitacion);


System.Collections.Generic.IList<HabitacionEN> Listar (int first, int size);


HabitacionEN Filtrar_por_id (int numero);
}
}
