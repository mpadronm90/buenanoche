
using System;
using BuenaNocheGenNHibernate.EN.Default_;

namespace BuenaNocheGenNHibernate.CAD.Default_
{
public partial interface ILineaReservaCAD
{
LineaReservaEN ReadOIDDefault (int id);

int New_ (LineaReservaEN lineaReserva);

void Destroy (int id);


void Modify (LineaReservaEN lineaReserva);
}
}
