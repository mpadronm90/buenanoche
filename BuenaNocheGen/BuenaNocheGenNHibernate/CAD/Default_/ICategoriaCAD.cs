
using System;
using BuenaNocheGenNHibernate.EN.Default_;

namespace BuenaNocheGenNHibernate.CAD.Default_
{
public partial interface ICategoriaCAD
{
CategoriaEN ReadOIDDefault (string nombre);

string New_ (CategoriaEN categoria);

void Destroy (string nombre);


void Modify (CategoriaEN categoria);


System.Collections.Generic.IList<CategoriaEN> ReadAll (int first, int size);


CategoriaEN ReadOID (string nombre);
}
}
