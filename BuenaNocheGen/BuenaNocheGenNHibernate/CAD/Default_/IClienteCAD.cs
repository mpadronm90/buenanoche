
using System;
using BuenaNocheGenNHibernate.EN.Default_;

namespace BuenaNocheGenNHibernate.CAD.Default_
{
public partial interface IClienteCAD
{
ClienteEN ReadOIDDefault (string nif);

string New_ (ClienteEN cliente);

void Destroy (string nif);


void Modify (ClienteEN cliente);


ClienteEN ReadOID (string nif);


System.Collections.Generic.IList<ClienteEN> ReadAll (int first, int size);
}
}
