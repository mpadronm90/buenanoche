
using System;
using BuenaNocheGenNHibernate.EN.Default_;

namespace BuenaNocheGenNHibernate.CAD.Default_
{
public partial interface IDiaCAD
{
DiaEN ReadOIDDefault (Nullable<DateTime> fecha);

Nullable<DateTime> New_ (DiaEN dia);

void Destroy (Nullable<DateTime> fecha);
}
}
