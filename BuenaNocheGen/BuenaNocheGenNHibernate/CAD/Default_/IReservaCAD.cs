
using System;
using BuenaNocheGenNHibernate.EN.Default_;

namespace BuenaNocheGenNHibernate.CAD.Default_
{
public partial interface IReservaCAD
{
ReservaEN ReadOIDDefault (int cod);

int New_ (ReservaEN reserva);

void Destroy (int cod);


void Modify (ReservaEN reserva);


ReservaEN ReadOID (int cod);


System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.ReservaEN> ReadForDate (Nullable<DateTime> p_filter);


System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.ReservaEN> ReadForClient (string p_client);
}
}
