
using System;

namespace BuenaNocheGenNHibernate.EN.Default_
{
public partial class DiaEN
{
/**
 *
 */

private Nullable<DateTime> fecha;

/**
 *
 */

private System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.LineaReservaEN> lineasReservaDia;





public virtual Nullable<DateTime> Fecha {
        get { return fecha; } set { fecha = value;  }
}


public virtual System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.LineaReservaEN> LineasReservaDia {
        get { return lineasReservaDia; } set { lineasReservaDia = value;  }
}





public DiaEN()
{
        lineasReservaDia = new System.Collections.Generic.List<BuenaNocheGenNHibernate.EN.Default_.LineaReservaEN>();
}



public DiaEN(Nullable<DateTime> fecha, System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.LineaReservaEN> lineasReservaDia)
{
        this.init (fecha, lineasReservaDia);
}


public DiaEN(DiaEN dia)
{
        this.init (dia.Fecha, dia.LineasReservaDia);
}

private void init (Nullable<DateTime> fecha, System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.LineaReservaEN> lineasReservaDia)
{
        this.Fecha = fecha;


        this.LineasReservaDia = lineasReservaDia;
}

public override bool Equals (object obj)
{
        if (obj == null)
                return false;
        DiaEN t = obj as DiaEN;
        if (t == null)
                return false;
        if (Fecha.Equals (t.Fecha))
                return true;
        else
                return false;
}

public override int GetHashCode ()
{
        int hash = 13;

        hash += this.Fecha.GetHashCode ();
        return hash;
}
}
}
