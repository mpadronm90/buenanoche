
using System;

namespace BuenaNocheGenNHibernate.EN.Default_
{
public partial class HabitacionEN
{
/**
 *
 */

private int numero;

/**
 *
 */

private float superficie;

/**
 *
 */

private int piso;

/**
 *
 */

private System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.LineaReservaEN> lineasReservaHabitacion;

/**
 *
 */

private BuenaNocheGenNHibernate.EN.Default_.CategoriaEN categoria;





public virtual int Numero {
        get { return numero; } set { numero = value;  }
}


public virtual float Superficie {
        get { return superficie; } set { superficie = value;  }
}


public virtual int Piso {
        get { return piso; } set { piso = value;  }
}


public virtual System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.LineaReservaEN> LineasReservaHabitacion {
        get { return lineasReservaHabitacion; } set { lineasReservaHabitacion = value;  }
}


public virtual BuenaNocheGenNHibernate.EN.Default_.CategoriaEN Categoria {
        get { return categoria; } set { categoria = value;  }
}





public HabitacionEN()
{
        lineasReservaHabitacion = new System.Collections.Generic.List<BuenaNocheGenNHibernate.EN.Default_.LineaReservaEN>();
}



public HabitacionEN(int numero, float superficie, int piso, System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.LineaReservaEN> lineasReservaHabitacion, BuenaNocheGenNHibernate.EN.Default_.CategoriaEN categoria)
{
        this.init (numero, superficie, piso, lineasReservaHabitacion, categoria);
}


public HabitacionEN(HabitacionEN habitacion)
{
        this.init (habitacion.Numero, habitacion.Superficie, habitacion.Piso, habitacion.LineasReservaHabitacion, habitacion.Categoria);
}

private void init (int numero, float superficie, int piso, System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.LineaReservaEN> lineasReservaHabitacion, BuenaNocheGenNHibernate.EN.Default_.CategoriaEN categoria)
{
        this.Numero = numero;


        this.Superficie = superficie;

        this.Piso = piso;

        this.LineasReservaHabitacion = lineasReservaHabitacion;

        this.Categoria = categoria;
}

public override bool Equals (object obj)
{
        if (obj == null)
                return false;
        HabitacionEN t = obj as HabitacionEN;
        if (t == null)
                return false;
        if (Numero.Equals (t.Numero))
                return true;
        else
                return false;
}

public override int GetHashCode ()
{
        int hash = 13;

        hash += this.Numero.GetHashCode ();
        return hash;
}
}
}
