
using System;

namespace BuenaNocheGenNHibernate.EN.Default_
{
public partial class CategoriaEN
{
/**
 *
 */

private string nombre;

/**
 *
 */

private string descripcion;

/**
 *
 */

private float sumin;

/**
 *
 */

private float sumax;

/**
 *
 */

private System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.HabitacionEN> habitaciones;





public virtual string Nombre {
        get { return nombre; } set { nombre = value;  }
}


public virtual string Descripcion {
        get { return descripcion; } set { descripcion = value;  }
}


public virtual float Sumin {
        get { return sumin; } set { sumin = value;  }
}


public virtual float Sumax {
        get { return sumax; } set { sumax = value;  }
}


public virtual System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.HabitacionEN> Habitaciones {
        get { return habitaciones; } set { habitaciones = value;  }
}





public CategoriaEN()
{
        habitaciones = new System.Collections.Generic.List<BuenaNocheGenNHibernate.EN.Default_.HabitacionEN>();
}



public CategoriaEN(string nombre, string descripcion, float sumin, float sumax, System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.HabitacionEN> habitaciones)
{
        this.init (nombre, descripcion, sumin, sumax, habitaciones);
}


public CategoriaEN(CategoriaEN categoria)
{
        this.init (categoria.Nombre, categoria.Descripcion, categoria.Sumin, categoria.Sumax, categoria.Habitaciones);
}

private void init (string nombre, string descripcion, float sumin, float sumax, System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.HabitacionEN> habitaciones)
{
        this.Nombre = nombre;


        this.Descripcion = descripcion;

        this.Sumin = sumin;

        this.Sumax = sumax;

        this.Habitaciones = habitaciones;
}

public override bool Equals (object obj)
{
        if (obj == null)
                return false;
        CategoriaEN t = obj as CategoriaEN;
        if (t == null)
                return false;
        if (Nombre.Equals (t.Nombre))
                return true;
        else
                return false;
}

public override int GetHashCode ()
{
        int hash = 13;

        hash += this.Nombre.GetHashCode ();
        return hash;
}
}
}
