
using System;

namespace BuenaNocheGenNHibernate.EN.Default_
{
public partial class ReservaEN
{
/**
 *
 */

private int cod;

/**
 *
 */

private System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.LineaReservaEN> lineasReservaReservas;

/**
 *
 */

private BuenaNocheGenNHibernate.EN.Default_.ClienteEN cliente;





public virtual int Cod {
        get { return cod; } set { cod = value;  }
}


public virtual System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.LineaReservaEN> LineasReservaReservas {
        get { return lineasReservaReservas; } set { lineasReservaReservas = value;  }
}


public virtual BuenaNocheGenNHibernate.EN.Default_.ClienteEN Cliente {
        get { return cliente; } set { cliente = value;  }
}





public ReservaEN()
{
        lineasReservaReservas = new System.Collections.Generic.List<BuenaNocheGenNHibernate.EN.Default_.LineaReservaEN>();
}



public ReservaEN(int cod, System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.LineaReservaEN> lineasReservaReservas, BuenaNocheGenNHibernate.EN.Default_.ClienteEN cliente)
{
        this.init (cod, lineasReservaReservas, cliente);
}


public ReservaEN(ReservaEN reserva)
{
        this.init (reserva.Cod, reserva.LineasReservaReservas, reserva.Cliente);
}

private void init (int cod, System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.LineaReservaEN> lineasReservaReservas, BuenaNocheGenNHibernate.EN.Default_.ClienteEN cliente)
{
        this.Cod = cod;


        this.LineasReservaReservas = lineasReservaReservas;

        this.Cliente = cliente;
}

public override bool Equals (object obj)
{
        if (obj == null)
                return false;
        ReservaEN t = obj as ReservaEN;
        if (t == null)
                return false;
        if (Cod.Equals (t.Cod))
                return true;
        else
                return false;
}

public override int GetHashCode ()
{
        int hash = 13;

        hash += this.Cod.GetHashCode ();
        return hash;
}
}
}
