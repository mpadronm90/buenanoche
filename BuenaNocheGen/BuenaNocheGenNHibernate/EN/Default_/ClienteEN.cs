
using System;

namespace BuenaNocheGenNHibernate.EN.Default_
{
public partial class ClienteEN
{
/**
 *
 */

private string nif;

/**
 *
 */

private string nombre;

/**
 *
 */

private string telefono;

/**
 *
 */

private string localidad;

/**
 *
 */

private string provincia;

/**
 *
 */

private string pais;

/**
 *
 */

private System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.ReservaEN> reservas;





public virtual string Nif {
        get { return nif; } set { nif = value;  }
}


public virtual string Nombre {
        get { return nombre; } set { nombre = value;  }
}


public virtual string Telefono {
        get { return telefono; } set { telefono = value;  }
}


public virtual string Localidad {
        get { return localidad; } set { localidad = value;  }
}


public virtual string Provincia {
        get { return provincia; } set { provincia = value;  }
}


public virtual string Pais {
        get { return pais; } set { pais = value;  }
}


public virtual System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.ReservaEN> Reservas {
        get { return reservas; } set { reservas = value;  }
}





public ClienteEN()
{
        reservas = new System.Collections.Generic.List<BuenaNocheGenNHibernate.EN.Default_.ReservaEN>();
}



public ClienteEN(string nif, string nombre, string telefono, string localidad, string provincia, string pais, System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.ReservaEN> reservas)
{
        this.init (nif, nombre, telefono, localidad, provincia, pais, reservas);
}


public ClienteEN(ClienteEN cliente)
{
        this.init (cliente.Nif, cliente.Nombre, cliente.Telefono, cliente.Localidad, cliente.Provincia, cliente.Pais, cliente.Reservas);
}

private void init (string nif, string nombre, string telefono, string localidad, string provincia, string pais, System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.ReservaEN> reservas)
{
        this.Nif = nif;


        this.Nombre = nombre;

        this.Telefono = telefono;

        this.Localidad = localidad;

        this.Provincia = provincia;

        this.Pais = pais;

        this.Reservas = reservas;
}

public override bool Equals (object obj)
{
        if (obj == null)
                return false;
        ClienteEN t = obj as ClienteEN;
        if (t == null)
                return false;
        if (Nif.Equals (t.Nif))
                return true;
        else
                return false;
}

public override int GetHashCode ()
{
        int hash = 13;

        hash += this.Nif.GetHashCode ();
        return hash;
}
}
}
