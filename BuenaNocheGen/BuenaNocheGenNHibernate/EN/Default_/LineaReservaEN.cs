
using System;

namespace BuenaNocheGenNHibernate.EN.Default_
{
public partial class LineaReservaEN
{
/**
 *
 */

private int id;

/**
 *
 */

private BuenaNocheGenNHibernate.EN.Default_.DiaEN dia;

/**
 *
 */

private BuenaNocheGenNHibernate.EN.Default_.HabitacionEN habitacion;

/**
 *
 */

private BuenaNocheGenNHibernate.EN.Default_.ReservaEN reserva;





public virtual int Id {
        get { return id; } set { id = value;  }
}


public virtual BuenaNocheGenNHibernate.EN.Default_.DiaEN Dia {
        get { return dia; } set { dia = value;  }
}


public virtual BuenaNocheGenNHibernate.EN.Default_.HabitacionEN Habitacion {
        get { return habitacion; } set { habitacion = value;  }
}


public virtual BuenaNocheGenNHibernate.EN.Default_.ReservaEN Reserva {
        get { return reserva; } set { reserva = value;  }
}





public LineaReservaEN()
{
}



public LineaReservaEN(int id, BuenaNocheGenNHibernate.EN.Default_.DiaEN dia, BuenaNocheGenNHibernate.EN.Default_.HabitacionEN habitacion, BuenaNocheGenNHibernate.EN.Default_.ReservaEN reserva)
{
        this.init (id, dia, habitacion, reserva);
}


public LineaReservaEN(LineaReservaEN lineaReserva)
{
        this.init (lineaReserva.Id, lineaReserva.Dia, lineaReserva.Habitacion, lineaReserva.Reserva);
}

private void init (int id, BuenaNocheGenNHibernate.EN.Default_.DiaEN dia, BuenaNocheGenNHibernate.EN.Default_.HabitacionEN habitacion, BuenaNocheGenNHibernate.EN.Default_.ReservaEN reserva)
{
        this.Id = id;


        this.Dia = dia;

        this.Habitacion = habitacion;

        this.Reserva = reserva;
}

public override bool Equals (object obj)
{
        if (obj == null)
                return false;
        LineaReservaEN t = obj as LineaReservaEN;
        if (t == null)
                return false;
        if (Id.Equals (t.Id))
                return true;
        else
                return false;
}

public override int GetHashCode ()
{
        int hash = 13;

        hash += this.Id.GetHashCode ();
        return hash;
}
}
}
