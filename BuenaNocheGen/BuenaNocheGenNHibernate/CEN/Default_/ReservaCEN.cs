

using System;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;

using BuenaNocheGenNHibernate.EN.Default_;
using BuenaNocheGenNHibernate.CAD.Default_;

namespace BuenaNocheGenNHibernate.CEN.Default_
{
public partial class ReservaCEN
{
private IReservaCAD _IReservaCAD;

public ReservaCEN()
{
        this._IReservaCAD = new ReservaCAD ();
}

public ReservaCEN(IReservaCAD _IReservaCAD)
{
        this._IReservaCAD = _IReservaCAD;
}

public IReservaCAD get_IReservaCAD ()
{
        return this._IReservaCAD;
}

public int New_ ()
{
        ReservaEN reservaEN = null;
        int oid;

        //Initialized ReservaEN
        reservaEN = new ReservaEN ();
        //Call to ReservaCAD

        oid = _IReservaCAD.New_ (reservaEN);
        return oid;
}

public void Destroy (int cod)
{
        _IReservaCAD.Destroy (cod);
}

public void Modify (int cod)
{
        ReservaEN reservaEN = null;

        //Initialized ReservaEN
        reservaEN = new ReservaEN ();
        reservaEN.Cod = cod;
        //Call to ReservaCAD

        _IReservaCAD.Modify (reservaEN);
}

public ReservaEN ReadOID (int cod)
{
        ReservaEN reservaEN = null;

        reservaEN = _IReservaCAD.ReadOID (cod);
        return reservaEN;
}

public System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.ReservaEN> ReadForDate (Nullable<DateTime> p_filter)
{
        return _IReservaCAD.ReadForDate (p_filter);
}
public System.Collections.Generic.IList<BuenaNocheGenNHibernate.EN.Default_.ReservaEN> ReadForClient (string p_client)
{
        return _IReservaCAD.ReadForClient (p_client);
}
}
}
