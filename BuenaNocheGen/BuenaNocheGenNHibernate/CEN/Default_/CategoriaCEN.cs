

using System;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;

using BuenaNocheGenNHibernate.EN.Default_;
using BuenaNocheGenNHibernate.CAD.Default_;

namespace BuenaNocheGenNHibernate.CEN.Default_
{
public partial class CategoriaCEN
{
private ICategoriaCAD _ICategoriaCAD;

public CategoriaCEN()
{
        this._ICategoriaCAD = new CategoriaCAD ();
}

public CategoriaCEN(ICategoriaCAD _ICategoriaCAD)
{
        this._ICategoriaCAD = _ICategoriaCAD;
}

public ICategoriaCAD get_ICategoriaCAD ()
{
        return this._ICategoriaCAD;
}

public string New_ (string p_nombre, string p_descripcion, float p_sup_min, float p_sup_max)
{
        CategoriaEN categoriaEN = null;
        string oid;

        //Initialized CategoriaEN
        categoriaEN = new CategoriaEN ();
        categoriaEN.Nombre = p_nombre;

        categoriaEN.Descripcion = p_descripcion;

        categoriaEN.Sumin = p_sup_min;

        categoriaEN.Sumax = p_sup_max;

        //Call to CategoriaCAD

        oid = _ICategoriaCAD.New_ (categoriaEN);
        return oid;
}

public void Destroy (string nombre)
{
        _ICategoriaCAD.Destroy (nombre);
}

public void Modify (string p_oid, string p_descripcion, float p_sup_min, float p_sup_max)
{
        CategoriaEN categoriaEN = null;

        //Initialized CategoriaEN
        categoriaEN = new CategoriaEN ();
        categoriaEN.Nombre = p_oid;
        categoriaEN.Descripcion = p_descripcion;
        categoriaEN.Sumin = p_sup_min;
        categoriaEN.Sumax = p_sup_max;
        //Call to CategoriaCAD

        _ICategoriaCAD.Modify (categoriaEN);
}

public System.Collections.Generic.IList<CategoriaEN> ReadAll (int first, int size)
{
        System.Collections.Generic.IList<CategoriaEN> list = null;

        list = _ICategoriaCAD.ReadAll (first, size);
        return list;
}
public CategoriaEN ReadOID (string nombre)
{
        CategoriaEN categoriaEN = null;

        categoriaEN = _ICategoriaCAD.ReadOID (nombre);
        return categoriaEN;
}
}
}
