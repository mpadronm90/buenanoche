

using System;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;

using BuenaNocheGenNHibernate.EN.Default_;
using BuenaNocheGenNHibernate.CAD.Default_;

namespace BuenaNocheGenNHibernate.CEN.Default_
{
public partial class HabitacionCEN
{
private IHabitacionCAD _IHabitacionCAD;

public HabitacionCEN()
{
        this._IHabitacionCAD = new HabitacionCAD ();
}

public HabitacionCEN(IHabitacionCAD _IHabitacionCAD)
{
        this._IHabitacionCAD = _IHabitacionCAD;
}

public IHabitacionCAD get_IHabitacionCAD ()
{
        return this._IHabitacionCAD;
}

public int Nueva (float superficie, int piso)
{
        HabitacionEN habitacionEN = null;
        int oid;

        //Initialized HabitacionEN
        habitacionEN = new HabitacionEN ();
        habitacionEN.Superficie = superficie;

        habitacionEN.Piso = piso;

        //Call to HabitacionCAD

        oid = _IHabitacionCAD.Nueva (habitacionEN);
        return oid;
}

public void Eliminar (int numero)
{
        _IHabitacionCAD.Eliminar (numero);
}

public void Modify (int p_oid, float p_superficie, int p_piso)
{
        HabitacionEN habitacionEN = null;

        //Initialized HabitacionEN
        habitacionEN = new HabitacionEN ();
        habitacionEN.Numero = p_oid;
        habitacionEN.Superficie = p_superficie;
        habitacionEN.Piso = p_piso;
        //Call to HabitacionCAD

        _IHabitacionCAD.Modify (habitacionEN);
}

public System.Collections.Generic.IList<HabitacionEN> Listar (int first, int size)
{
        System.Collections.Generic.IList<HabitacionEN> list = null;

        list = _IHabitacionCAD.Listar (first, size);
        return list;
}
public HabitacionEN Filtrar_por_id (int numero)
{
        HabitacionEN habitacionEN = null;

        habitacionEN = _IHabitacionCAD.Filtrar_por_id (numero);
        return habitacionEN;
}
}
}
