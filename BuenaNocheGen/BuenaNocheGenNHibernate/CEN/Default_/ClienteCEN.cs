

using System;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;

using BuenaNocheGenNHibernate.EN.Default_;
using BuenaNocheGenNHibernate.CAD.Default_;

namespace BuenaNocheGenNHibernate.CEN.Default_
{
public partial class ClienteCEN
{
private IClienteCAD _IClienteCAD;

public ClienteCEN()
{
        this._IClienteCAD = new ClienteCAD ();
}

public ClienteCEN(IClienteCAD _IClienteCAD)
{
        this._IClienteCAD = _IClienteCAD;
}

public IClienteCAD get_IClienteCAD ()
{
        return this._IClienteCAD;
}

public string New_ (string p_nif, string p_nombre, string p_telefono, string p_localidad, string p_provincia, string p_pais)
{
        ClienteEN clienteEN = null;
        string oid;

        //Initialized ClienteEN
        clienteEN = new ClienteEN ();
        clienteEN.Nif = p_nif;

        clienteEN.Nombre = p_nombre;

        clienteEN.Telefono = p_telefono;

        clienteEN.Localidad = p_localidad;

        clienteEN.Provincia = p_provincia;

        clienteEN.Pais = p_pais;

        //Call to ClienteCAD

        oid = _IClienteCAD.New_ (clienteEN);
        return oid;
}

public void Destroy (string nif)
{
        _IClienteCAD.Destroy (nif);
}

public void Modify (string p_oid, string p_nombre, string p_telefono, string p_localidad, string p_provincia, string p_pais)
{
        ClienteEN clienteEN = null;

        //Initialized ClienteEN
        clienteEN = new ClienteEN ();
        clienteEN.Nif = p_oid;
        clienteEN.Nombre = p_nombre;
        clienteEN.Telefono = p_telefono;
        clienteEN.Localidad = p_localidad;
        clienteEN.Provincia = p_provincia;
        clienteEN.Pais = p_pais;
        //Call to ClienteCAD

        _IClienteCAD.Modify (clienteEN);
}

public ClienteEN ReadOID (string nif)
{
        ClienteEN clienteEN = null;

        clienteEN = _IClienteCAD.ReadOID (nif);
        return clienteEN;
}

public System.Collections.Generic.IList<ClienteEN> ReadAll (int first, int size)
{
        System.Collections.Generic.IList<ClienteEN> list = null;

        list = _IClienteCAD.ReadAll (first, size);
        return list;
}
}
}
