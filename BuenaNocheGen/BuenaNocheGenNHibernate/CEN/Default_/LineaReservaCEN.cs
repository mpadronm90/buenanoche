

using System;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;

using BuenaNocheGenNHibernate.EN.Default_;
using BuenaNocheGenNHibernate.CAD.Default_;

namespace BuenaNocheGenNHibernate.CEN.Default_
{
public partial class LineaReservaCEN
{
private ILineaReservaCAD _ILineaReservaCAD;

public LineaReservaCEN()
{
        this._ILineaReservaCAD = new LineaReservaCAD ();
}

public LineaReservaCEN(ILineaReservaCAD _ILineaReservaCAD)
{
        this._ILineaReservaCAD = _ILineaReservaCAD;
}

public ILineaReservaCAD get_ILineaReservaCAD ()
{
        return this._ILineaReservaCAD;
}

public int New_ ()
{
        LineaReservaEN lineaReservaEN = null;
        int oid;

        //Initialized LineaReservaEN
        lineaReservaEN = new LineaReservaEN ();
        //Call to LineaReservaCAD

        oid = _ILineaReservaCAD.New_ (lineaReservaEN);
        return oid;
}

public void Destroy (int id)
{
        _ILineaReservaCAD.Destroy (id);
}

public void Modify (int p_oid)
{
        LineaReservaEN lineaReservaEN = null;

        //Initialized LineaReservaEN
        lineaReservaEN = new LineaReservaEN ();
        lineaReservaEN.Id = p_oid;
        //Call to LineaReservaCAD

        _ILineaReservaCAD.Modify (lineaReservaEN);
}
}
}
