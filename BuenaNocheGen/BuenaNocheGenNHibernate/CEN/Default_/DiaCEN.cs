

using System;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;

using BuenaNocheGenNHibernate.EN.Default_;
using BuenaNocheGenNHibernate.CAD.Default_;

namespace BuenaNocheGenNHibernate.CEN.Default_
{
public partial class DiaCEN
{
private IDiaCAD _IDiaCAD;

public DiaCEN()
{
        this._IDiaCAD = new DiaCAD ();
}

public DiaCEN(IDiaCAD _IDiaCAD)
{
        this._IDiaCAD = _IDiaCAD;
}

public IDiaCAD get_IDiaCAD ()
{
        return this._IDiaCAD;
}

public Nullable<DateTime> New_ (Nullable<DateTime> p_fecha)
{
        DiaEN diaEN = null;

        Nullable<DateTime> oid;
        //Initialized DiaEN
        diaEN = new DiaEN ();
        diaEN.Fecha = p_fecha;

        //Call to DiaCAD

        oid = _IDiaCAD.New_ (diaEN);
        return oid;
}

public void Destroy (Nullable<DateTime> fecha)
{
        _IDiaCAD.Destroy (fecha);
}
}
}
