
using System;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;

using BuenaNocheGenNHibernate.EN.Default_;
using BuenaNocheGenNHibernate.CAD.Default_;

namespace BuenaNocheGenNHibernate.CEN.Default_
{
public partial class HabitacionCEN
{
public void Modify (int p_habitacion_OID, float p_superficie, int p_piso, string p_categoria)
{
        /*PROTECTED REGION ID(BuenaNocheGenNHibernate.CEN.Default__habitacion_modify_customized) START*/

        HabitacionEN habitacionEN = null;

        //Initialized HabitacionEN
        habitacionEN = new HabitacionEN ();
        habitacionEN.Numero = p_habitacion_OID;
        habitacionEN.Superficie = p_superficie;
        habitacionEN.Piso = p_piso;
        habitacionEN.Categoria = p_categoria;
        //Call to HabitacionCAD

        _IHabitacionCAD.Modify (habitacionEN);

        /*PROTECTED REGION END*/
}
}
}
