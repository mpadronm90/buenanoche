﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using BuenanocheGenNHibernate.EN.Default_;
using BuenanocheGenNHibernate.CAD.Default_;

namespace BuenaNocheFachada
{
    
    class NuevaReserva
    {
        private ReservaCP reservaCP;
        private IClienteCAD clienteCAD;

        public System.Collections.Generic.IList<ClienteEN> dameCliDNI(string dni){
            System.Collections.Generic.IList<ClienteEN> clientes = null;
            clientes = reservaCP.dameCliDNI(dni);
            return clientes;

        }

        public void anyadirReserva(ReservaEN reserva, ClienteEN cliente){
            reservaCP.anyadirReserva(reserva,cliente);
        }

}
