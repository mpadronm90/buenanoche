﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuenaNocheGenNHibernate.CEN.Default_;

namespace BuenaNocheFachada
{
    /**
     * INTERFACE DE PROTOTIPOS PARA LA SOLUCIÓN DE INTERFAZ CARA AL USUARIO FINAL
     * Si la función devuelve Boolean:
     *   En caso de error en base de datos, return false.
     * Si la función devuelve lista u objeto:
     *   En caso de error en base de datos, return null.
     * Si la función devuelve una lista:
     *   En caso de que no haya coincidencias en la búsqueda, return lista vacía.
     * Si la función devuelve un objeto:
     *   En caso de que no haya coincidencias en la búsqueda, return null.
     */
    public interface Class1
    {
        /** 
         * Lista las habitaciones.
         * @OrderBy Categoria : individuales,dobles y suites.
         * @return Lista de Habitaciones
         */

        // Michel completa


        public List<HabitacionCEN> listadoHabitaciones();

        /**
         * Recupera una lista de clientes en cuyo dni esté contenido o
         * sea en su totalidad el parámetro 'dniParcial'
         * @param dniParcial : String
         * @OrderBy Nombre : ascendente alfabeticamente
         * @return Lista de Clientes
         */
        //Dario
        public List<ClienteCEN> dameClienteDNIParcial(String dniParcial);

        /**
         * Recupera una lista de los cliente con el nombre parámetro
         * @param nombre : String
         * @return Lista de Clientes
         */
        //Dario
        public List<ClienteCEN> dameClientePorNombre(String nombre);

        /**
         * Recupera un cliente a partir de su dni,
         * Deben coincidir los dni en su totalidad, cliente inequívoco
         * @param dni : String
         * @return ClienteCEN
         */
        //Dario
        public ClienteCEN dameClientePorDNI(String dni);

         /**
          * Introduce un cliente nuevo en la base de datos.
          * @param nif : String -> OID
          * @param nombre : String
          * @param telefono : String
          * @param localidad : String
          * @param provincia : String
          * @param pais : String
          * @return booleano : si se introdujo o no.
          */
        //Dario
        public Boolean guardaNuevoCliente(string nif, string nombre, string telefono, string localidad, string provincia, string pais);
        /**
          * Edita un cliente en la base de datos.
          * Si el nif no se encuentra, return false.
          * @param nif : String -> OID #NO SE DEBE ALTERAR
          * @param nombre : String
          * @param telefono : String
          * @param localidad : String
          * @param provincia : String
          * @param pais : String
          * @return booleano : si se editó o no.
          */
        //dario
        public Boolean guardaCliente(string nif, string nombre, string telefono, string localidad, string provincia, string pais);

        /**
         * Edita el nif de un cliente ya existente en la base de datos.
         * Si el nifAntiguo no se encuentra, return false.
         * @param nifAntiguo : String
         * @param nifNuevo : String
         * @return booleano : si se editó o no.
         */
        //dario
        public Boolean modificaNifCliente(String nifAntiguo, String nifNuevo);

        /**
         * Introduce una nueva reserva en la base de datos.
         * No se debe introducir una reserva de una habitación que está ocupada
         *   entre intervalos fechaEntrada y fechaSalida de una reserva anterior.
         * @param cliente : ClienteCEN
         * @param fecha : DateTime
         * @param habitacion : HabitacionCEN
         * @return booleano : si se introdujo o no.
         */
        //dario
        public Boolean guardaReserva(ClienteCEN cliente, DateTime fechaEntrada,DateTime fechaSalida, HabitacionCEN habitacion);

        /**
         * Busca una reserva concreta por clave
         * @param num : int
         * @return ReservaCEN
         */

        //michel completa


        public ReservaCEN dameReservaPorOID(int num);

        /**
         * Busca las reservas que haya realizado un cliente.
         * Del cliente se sabe un String, este puede ser nombre o dni.
         * @param nombre_dni : String
         * @OrderBy fecha : descendente, de la más reciente a la más antigua.
         * @return Lista de Reservas hechas por el cliente.
         */

        //michel completa
        public List<ReservaCEN> dameReservasPorCliente(String nombre_dni);

        /**
         * Busca las reservas que tengan entrada la fecha parámetro
         * @param fecha : DateTime
         * @OrderBy nombre : del cliente que ha hecho la reserva, ascendente alfabético.
         * @return Lista de Reservas con entrada una fecha
         */
        //michel
        public List<ReservaCEN> dameReservasPorFecha(DateTime fecha);

        /**
         * Devuelve una habitación por su número.
         * @param num : int
         * @return HabitacionCEN
         */
        //michel completa
        public HabitacionCEN dameHabitacionPorNum(int num);

        /**
         * Cambia la categoria de una habitacion.
         * @param num : int
         * @param categoria : CategoriaCEN
         * @return booleano : si se ha cambiado o si no
         */
        //michel
        //Esta funcion no es posible implementarla por falta error encontrado en
        //nhibernate. Al tratar de customizar la operacion modify de habitacion
        //OOH4RIA solo customiza el CEN pero no lo hace en el CAD siendo imposible realizar el 
        //cambio para modificar su categoría
        public Boolean cambiaCategoriaHabitacion(int num, CategoriaCEN categoria);

    }
}
