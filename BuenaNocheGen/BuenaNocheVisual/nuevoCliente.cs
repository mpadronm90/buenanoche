﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BuenaNocheGenNHibernate.EN.Default_;
using BuenaNocheCP.CPs;

namespace BuenaNocheVisual
{
    public partial class nuevoCliente : Form
    {
        main theMain;
        public nuevoCliente(main m)
        {
            InitializeComponent();
            theMain = m;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "" || textBox6.Text == "")
            {
                return;
            }
            ClienteCP manejadorCliente = new ClienteCP();
            if (manejadorCliente.guardaNuevoCliente(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text, textBox6.Text))
            {
                MessageBox.Show("Cliente guardado correctamente.");
                this.Close();
            }
            else
            {
                MessageBox.Show("El cliente no se ha guardado correctamente.");
            }
        }

        private void nuevoCliente_FormClosing(object sender, FormClosingEventArgs e)
        {
            theMain.Show();
        }
    }
}
