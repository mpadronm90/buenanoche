﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BuenaNocheGenNHibernate.EN.Default_;
using BuenaNocheCP.CPs;

namespace BuenaNocheVisual
{
    public partial class editaCliente : Form
    {
        main theMain;
        String clienteInfo;
        public editaCliente(main m, String texto)
        {
            InitializeComponent();
            theMain = m;
            clienteInfo = texto;
        }

        private void editaCliente_Load(object sender, EventArgs e)
        {
            String[] texto = clienteInfo.Split(new Char[] {'-'});
            ClienteCP manejador = new ClienteCP();
            ClienteEN cliente = manejador.dameClientePorDNI(texto[0]);
            textBox1.Text = cliente.Nif;
            textBox2.Text = cliente.Nombre;
            textBox3.Text = cliente.Telefono;
            textBox4.Text = cliente.Localidad;
            textBox5.Text = cliente.Provincia;
            textBox6.Text = cliente.Pais;
        }

        private void editaCliente_FormClosing(object sender, FormClosingEventArgs e)
        {
            theMain.Show();
        }
    }
}
