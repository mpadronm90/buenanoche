﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using BuenaNocheGenNHibernate.EN.Default_;
using BuenaNocheCP.CPs;

namespace BuenaNocheVisual
{
    public partial class nuevaReserva : Form
    {
        main theMain;
        public nuevaReserva(main m)
        {
            InitializeComponent();
            theMain = m;
        }

        private void nuevaReserva_Load(object sender, EventArgs e)
        {

        }

        private void nuevaReserva_FormClosing(object sender, FormClosingEventArgs e)
        {
            theMain.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text=="" || textBox3.Text=="" || textBox4.Text=="")
            {
                return;
            }
            DateTime fechaEntrada = DateTime.ParseExact(textBox3.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime fechaSalida = DateTime.ParseExact(textBox4.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
            ClienteCP manejadorCliente = new ClienteCP();
            ClienteEN cliente = manejadorCliente.dameClientePorDNI(textBox1.Text);
            HabitacionCP manejadorHab = new HabitacionCP();
            HabitacionEN habitacion = manejadorHab.dameHabitacionPorNum(int.Parse(textBox2.Text));
            ReservaCP manejadorReserva = new ReservaCP();
            if(manejadorReserva.guardaReserva(cliente,fechaEntrada,fechaSalida,habitacion))
            {
                MessageBox.Show("Se ha guardado la reserva correctamente.");
                this.Close();
            }
            else
            {
                MessageBox.Show("La reserva no se ha guardado.");
            }
        }
    }
}
