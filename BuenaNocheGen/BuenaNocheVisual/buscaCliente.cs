﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BuenaNocheCP.CPs;
using BuenaNocheGenNHibernate.EN.Default_;

namespace BuenaNocheVisual
{
    public partial class buscaCliente : Form
    {
        main theMain;
        bool edita;
        public buscaCliente(main m)
        {
            InitializeComponent();
            theMain = m;
            edita = false;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String clienteText = textBox1.Text;
            ClienteCP clienteCP = new ClienteCP();
            List<ClienteEN> listaClientes = clienteCP.dameClientePorNombre(clienteText);
            foreach (ClienteEN cli in listaClientes)
            {
                comboBox1.Items.Add(cli.Nif+"-"+cli.Nombre);
            }
        }

        private void buscaCliente_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!edita)
            {
                theMain.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String clienteText = comboBox1.SelectedText;
            if (clienteText.Length > 0)
            {
                edita = true;
                editaCliente ec = new editaCliente(theMain, clienteText);
                ec.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Selecciona algún cliente.");
            }
        }
    }
}
