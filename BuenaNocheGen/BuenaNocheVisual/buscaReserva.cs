﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BuenaNocheVisual
{
    public partial class buscaReserva : Form
    {
        editaReserva super;
        public buscaReserva(int idReserva,editaReserva er)
        {
            InitializeComponent();
            label1.Text = "Se entró buscando por la id de reserva.";
            super = er;
        }
        public buscaReserva(String idCliente,editaReserva er)
        {
            InitializeComponent();
            label1.Text = "Se entró buscando por cliente.";
            super = er;
        }
        public buscaReserva(DateTime fecha,editaReserva er)
        {
            InitializeComponent();
            label1.Text = "Se entró buscando por la fecha de entrada.";
            super = er;
        }

        private void buscaReserva_Load(object sender, EventArgs e)
        {

        }

        private void buscaReserva_FormClosing(object sender, FormClosingEventArgs e)
        {
            super.Show();
        }

    }
}
