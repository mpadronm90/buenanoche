﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BuenaNocheVisual
{
    public partial class main : Form
    {
        public main()
        {
            InitializeComponent();
            button1.FlatAppearance.BorderSize = 0;
            button2.FlatAppearance.BorderSize = 0;
            button3.FlatAppearance.BorderSize = 0;
            button4.FlatAppearance.BorderSize = 0;
            button5.FlatAppearance.BorderSize = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            nuevaReserva nr = new nuevaReserva(this);
            nr.Show();
            esconderForm();
        }
        private void button1_MouseHover(object sender, EventArgs e)
        {
            this.button1.BackColor = Color.Transparent;
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void esconderForm()
        {
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            editaReserva er = new editaReserva(this);
            er.Show();
            esconderForm();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            nuevoCliente nc = new nuevoCliente(this);
            nc.Show();
            esconderForm();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            buscaCliente bc = new buscaCliente(this);
            bc.Show();
            esconderForm();
        }
    }
}
